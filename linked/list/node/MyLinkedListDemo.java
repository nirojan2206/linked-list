package bcas.dsa.linked.list.node;

public class MyLinkedListDemo {

	
	public static void main(String[] args) {
		
		MyLinkedList mylink = new MyLinkedList();
		mylink.display();
		
		mylink.insertAtStart(50);
		mylink.display();
	
		mylink.insertAtStart(500);
		mylink.display();
		
		mylink.insertAtStart(5000);
		mylink.display();
		
		mylink.insertAtEnd(5);
		mylink.display();
		
		mylink.insertAtPos(100, 4);
		mylink.display();
		
		
		mylink.deleteAtPos(50,3);
		mylink.display();
	}
}
