package bcas.dsa.linked.list.node;

public class MyLinkedList {

	private Node start;
	private Node end;
	public int size;

	public MyLinkedList() {

		start = null;
		end = null;
		size = 0;
	}

	public void insertAtStart(int val) {

		Node node = new Node(val, null);
		size++;

		if (start == null) {
			start = node;
			end = start;

		} else {

			node.setLink(start);
			start = node;
		}
	}

	
	public void insertAtEnd(int val) {

		Node node = new Node(val, null);
		size++;

		if (start == null) {
			start = node;
			end = start;

		} else {

			end.setLink(node);
			end = node;
		}
	}
	
	
	public void insertAtPos (int val , int pos) {
		
		Node nptr = new Node(val, null);
		Node ptr = start;
		pos = pos - 1;
		
		for (int i = 1; i<size; i++) {
			
			if (i == pos) {
				
				Node tmp = ptr.getLink();
				ptr.setLink(nptr);
				nptr.setLink(tmp);
				break;
			}
			ptr = ptr.getLink();
		}
		size ++;
	}
	
public void deleteAtPos (int val , int pos) {
		
		Node ptr = start;
		pos = pos - 1;
		
		for (int i = 1; i<size - 1; i++) {
			
			if (i == pos) {
				
				Node tmp = ptr.getLink();
				tmp = tmp.getLink();		
				ptr.setLink(tmp);
				break;
			}
			ptr = ptr.getLink();
		}
		size --;
	}
	
	
	public void display() {

		System.out.print(" \n Singly Link List = ");

		if (size == 0) {

			System.out.print("Empty ");
			return;
		}
		if (start.getLink() == null) {
			System.out.print(start.getData());
			return;

		}

		Node node = start;
		System.out.print(start.getData() + " ->> ");
		node = start.getLink();
		while (node.getLink() != null) {
			System.out.print(node.getData() + " ->> ");
			node = node.getLink();
		}
			System.out.print(node.getData());
		
		}

	}

